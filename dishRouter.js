/*
Part of Assignment 1 of Server-Side Development course by HKUST


Task 1 of 3 Tasks

In this task you will create a separate Node module implementing an Express router to support the REST API for the dishes. You can reuse all the code that you implemented in the previous exercise. To do this, you need to complete the following:

Create a Node module named dishRouter.js that implements the Express router for the /dishes REST API end point.
Require the Node module you create above within your Express application and mount it on the /dishes route.
*/

// all 3 of these modules need to be installed
var express = require('express');
//var morgan = require('morgan');
var bodyParser = require('body-parser');

//var hostname = 'localhost';
//var port = 3000;

//var app = express();

//app.use(morgan('dev'));
// use the Express router in this Express server app
var dishRouter = express.Router();
// json strings get parsed and are available as req.body
dishRouter.use(bodyParser.json());

// make this module available for export to server.js module
module.exports = dishRouter;

// Define a dishRouter for the top address localhost:3000/dishes/
// for any route after the host address, chain together all, get, post, delete
// A PUT request to this route will get stuck with a 200 ok message but no end to the response,
// because .all is defined with a next(); but put is not defined with res.end(); to end the 
// response message.  Here .put should not be defined because put is update, and there's no dish
// to update at this route.
dishRouter.route('/')
.all(function(req,res,next) {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      next();
})

.get(function(req,res,next){
        res.end('Will send all the dishes to you!');
})

//post a new dish to dishes/ route and pass the dishId in the JS object sent in the body of the 
// request.  The new dish can be put in the db this way.  No need to POST to dishes/dishId.
.post(function(req, res, next){
    res.end('Will add the dish: ' + req.body.name + ' with details: ' + req.body.description);    
})

.delete(function(req, res, next){
        res.end('Deleting all dishes');
});



// define another router for any dishId route
dishRouter.route('/:id')
.all(function(req,res,next) {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      next();
})

.get(function(req,res,next){
        res.end('Will send details of the dish: ' + req.params.dishId +' to you!');
})

.put(function(req, res, next){
        res.write('Updating the dish: ' + req.params.dishId + '\n');
    res.end('Will update the dish: ' + req.body.name + 
            ' with details: ' + req.body.description);
})

.delete(function(req, res, next){
        res.end('Deleting dish: ' + req.params.dishId);
});
