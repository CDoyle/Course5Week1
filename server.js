/*
Server-Side Development course HKUST
Assignment1 
Assignment Overview

In this assignment you will continue the exploration of Node modules, Express and the REST API. At the end of this assignment, you should have completed the following tasks to update the server:

Created a Node module using Express router to support the routes for the dishes REST API.
Created a Node module using Express router to support the routes for the promotions REST API.
Created a Node module using Express router to support the routes for the leadership REST API.
Assignment Requirements

The REST API for our Angular and Ionic/Cordova application that we built in the previous courses requires us to support the following REST API end points:

http://localhost:3000/dishes
http://localhost:3000/promotions
http://localhost:3000/leadership
We need to support GET, PUT, POST and DELETE operations on each of the three endpoints mentioned above, including supporting the use of route parameters to identify a specific dish, promotion and leader. We have already constructed the REST API for the dishes route in the previous exercise.

This assignment requires you to complete the following three tasks. Detailed instructions for each task are given below.

Task 1

In this task you will create a separate Node module implementing an Express router to support the REST API for the dishes. You can reuse all the code that you implemented in the previous exercise. To do this, you need to complete the following:

Create a Node module named dishRouter.js that implements the Express router for the /dishes REST API end point.
Require the Node module you create above within your Express application and mount it on the /dishes route.
Task 2

In this task you will create a separate Node module implementing an Express router to support the REST API for the promotions. To do this, you need to complete the following:

Create a Node module named promoRouter.js that implements the Express router for the /promotions REST API end point.
Require the Node module you create above within your Express application and mount it on the /promotions route.
Task 3

In this task you will create a separate Node module implementing an Express router to support the REST API for the leadership. To do this, you need to complete the following:

Create a Node module named leaderRouter.js that implements the Express router for the /leadership REST API end point.
Require the Node module you create above within your Express application and mount it on the /leadership route.

*/

/*
Express server app which uses the Express Routers defined in dishRouter.js, promoRouter.js and 
*/


// import the dishRouter module, to be used as this server app's router
var dishRouter = require('./dishRouter');
var promoRouter = require('./promoRouter');
var leaderRouter = require('./leaderRouter');
var express = require('express');
// create an Express app instance
var app = express();
// morgan is used to print cmd-line debug messages for each request coming into this server
var morgan = require('morgan');

var hostname = 'localhost';
var port = 3000;

app.use(morgan('dev'));

// add /dishes to the base of the dishRouter, making the base address localhost:3000/dishes
app.use('/dishes',dishRouter);
// add /promotions to the base of the promoRouter address
app.use('/promotions', promoRouter);

app.use('/leadership', leaderRouter);

// Tell ExpressRouter to look in /public for static files like index.html
// define the route for static files in localhost:3000/public
// any .html file that can be found in /public can now be served
app.use(express.static(__dirname + '/public'));

// start the server
app.listen(port, hostname, function(){
  console.log(`Server running at http://${hostname}:${port}/`);
});